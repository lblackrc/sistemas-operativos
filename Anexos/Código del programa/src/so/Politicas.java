/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package so;

import java.util.ArrayList;

/**
 *
 * @author haobr
 */
public class Politicas {
    
    public Proceso FCFS(ArrayList<Proceso> ColaList ){

    return ColaList.get(0);
    }
        
    public Proceso SJF(ArrayList<Proceso> ColaList ){
        int menor=10000;
        int idmenor=0;
        for(Proceso p: ColaList){

            if(p.getBursttime()<menor){
                menor=p.getBursttime();
                idmenor=p.getId();
            }             
         } 

        for(Proceso p: ColaList){
 
            if(p.getId()==idmenor){
               // System.out.println("menor proceso: "+p.id +"  // burstime: "+p.bursttime + ColaList);
                return p;
            }             
         } 
   return null;             
    }    

    public Proceso Prioridades(ArrayList<Proceso> ColaList ){
        int mayor=0;
        int idmayor=0;
        for(Proceso p: ColaList){

            if(p.getPrioridad()>mayor){
                mayor=p.getPrioridad();
                idmayor=p.getId();
            }             
         } 

        for(Proceso p: ColaList){
 
            if(p.getId()==idmayor){
               // System.out.println(" proceso con mayor prioridad : "+p.id +"  // prioridad : "+p.prioridad + ColaList);
                return p;
            }             
         } 
   return null;             
    }    
    
public Proceso RounRobin(ArrayList<Proceso> ColaList ){

    return ColaList.get(0);
    }
    


}
