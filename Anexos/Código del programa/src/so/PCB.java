/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package so;

/**
 *
 * @author haobr
 */
public class PCB {
    
        int ProcesoPadre;
        int bursttime;
        int interrupciones;
        int prioridad;
        int id;
        int peso;
        int periodoInterrupcion;
        String estado;
        int tiempoArribo;
        int tiempoInterrupcion;
        String codError;
        int idDES;
        int PC;

    public PCB() {
        this.ProcesoPadre=0;
        this.bursttime = 0;
        this.interrupciones = 0;
        this.prioridad = 0;
        this.id = 0;
        this.peso = 0;
        this.periodoInterrupcion = 0;
        this.estado = "";
        this.tiempoArribo = 0;
        this.tiempoInterrupcion=0;
        this.codError="";
        this.idDES=0;
        this.PC=0;
    }

    public String getCodError() {
        return codError;
    }

    public void setCodError(String codError) {
        this.codError = codError;
    }

    public int getTiempoInterrupcion() {
        return tiempoInterrupcion;
    }

    public void setTiempoInterrupcion(int tiempoInterrupcion) {
        this.tiempoInterrupcion = tiempoInterrupcion;
    }

    public int getBursttime() {
        return bursttime;
    }

    public void setBursttime(int bursttime) {
        this.bursttime = bursttime;
    }

    public int getInterrupciones() {
        return interrupciones;
    }

    public void setInterrupciones(int interrupciones) {
        this.interrupciones = interrupciones;
    }

    public int getPrioridad() {
        return prioridad;
    }

    public void setPrioridad(int prioridad) {
        this.prioridad = prioridad;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPeso() {
        return peso;
    }

    public void setPeso(int peso) {
        this.peso = peso;
    }

    public int getPeriodoInterrupcion() {
        return periodoInterrupcion;
    }

    public void setPeriodoInterrupcion(int periodoInterrupcion) {
        this.periodoInterrupcion = periodoInterrupcion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public int getTiempoArribo() {
        return tiempoArribo;
    }

    public void setTiempoArribo(int tiempoArribo) {
        this.tiempoArribo = tiempoArribo;
    }

    int getPC() {
        return PC;
        
    }

    void setPC(int pc) {
        this.PC=pc;
    }

    @Override
    public String toString() {
        return "PCB{" +"ID:"+id+ "ProcesoPadre=" + ProcesoPadre + ", bursttime=" + bursttime + ", interrupciones=" + interrupciones + ", prioridad=" + prioridad + ", id=" + id + ", peso=" + peso + ", periodoInterrupcion=" + periodoInterrupcion + ", estado=" + estado + ", tiempoArribo=" + tiempoArribo + ", tiempoInterrupcion=" + tiempoInterrupcion + ", codError=" + codError + ", idDES=" + idDES + ", PC=" + PC + '}';
    }
        
}
