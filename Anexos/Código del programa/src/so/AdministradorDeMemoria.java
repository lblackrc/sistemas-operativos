/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package so;

import Random.random;
import java.util.ArrayList;

/**
 *
 * @author crfsy
 */
public class AdministradorDeMemoria {
            int base;
            
            public ArrayList<BloqueMemoria> AsignarBloque(ArrayList<Proceso> Cola, int i, ArrayList<BloqueMemoria> bloque, int estrategia){
                
            ArrayList<BloqueMemoria> BM=new ArrayList<>();
            Estrategias Estrateg= new Estrategias();
            random rand= new random();
            BloqueMemoria bloquem =new BloqueMemoria();
            BloqueMemoria bloquem2 =new BloqueMemoria();
                for(Proceso p: Cola){
                    if(i==p.tiempoArribo){
                        //System.out.println(bloque);
                       base=Estrateg.Estrategias(estrategia, p.peso, bloque);
                        //System.out.println(base+"   "+bloque);
                       bloquem.idproceso=p.id;
                       bloquem.base=base;
                       bloquem.longitud=p.peso;
                       bloquem.tipo=2;
                       bloquem.basecode=base;
                       bloquem.code=(int)(rand.uniforme1(0.1*p.peso,0.2*p.peso));
                       bloquem.basedata=bloquem.code+bloquem.basecode;
                       bloquem.data=(int)(0.3*p.peso-bloquem.code);
                       bloquem.basestack=bloquem.basedata+bloquem.data;
                       bloquem.stack=(int)(rand.uniforme1(0.4*p.peso, 0.5*p.peso));
                       bloquem.baseheap=bloquem.basestack+bloquem.stack;
                       bloquem.heap=bloquem.longitud-(bloquem.stack+bloquem.code+bloquem.data);
                        System.out.println("BLOQUEM----> "+bloquem);   
                     for(BloqueMemoria k: bloque){
                         if(k.base==base){
                            BM.add(bloquem);
                             System.out.println(BM);
                             if (k.longitud!=p.peso) {
                                 bloquem2.base=bloquem.base+bloquem.longitud;
                                 bloquem2.longitud  =k.longitud-bloquem.longitud;
                                 bloquem2.tipo=1;
                                 
                             }
                             BM.add(bloquem2);
                         
                         }else {
                         BM.add(k);
                         
                         } 
                     
                     }  
                    
                    
                    return BM;
                    }
                } 
       
 return bloque;
}

            
public ArrayList<BloqueMemoria> LiberarBloque(  ArrayList<BloqueMemoria> bloque,int idProceso){
            ArrayList<BloqueMemoria> BM=new ArrayList<>();
                for(BloqueMemoria p: bloque){
                    if(p.idproceso==idProceso){
                       p.tipo=1;
                       
                    }
                BM.add(p);
                } 
            
       return BM;     
}

public ArrayList<BloqueMemoria> UnirBloque(  ArrayList<BloqueMemoria> bloque){
            ArrayList<BloqueMemoria> BM=new ArrayList<>();
            ArrayList<Integer> Indicadores=new ArrayList<>();
            BloqueMemoria BQM=new BloqueMemoria();
            boolean t=true;
            boolean ñ=false;
            int longitudes=0;
            int cont=1;
            int tamaño=0;
                for(BloqueMemoria p: bloque){
                    tamaño=tamaño+1;
                  
                   t=true; 
                   if(p.tipo==1){
                       Indicadores.add(p.base);
                       Indicadores.add(p.longitud);
                       //System.out.println("asignando longitud::    "+p.longitud);
                       t=false; 
                       ñ=true;
                          //System.out.println("tipo hueco    ::"+Indicadores);
                   }else{
                       if(ñ){
                       for(Integer j:Indicadores){
                          // System.out.println(Indicadores);
                           if(cont%2==0){
                            longitudes=j+longitudes;
                              // System.out.println("aumentar    ::"+longitudes);
                           
                           }
                       cont=cont+1;
                       }
                       BQM.base=Indicadores.get(0);
                          //System.out.println("asignar    ::"+longitudes);
                       BQM.longitud=longitudes;
                          //System.out.println(BQM.longitud);
                       BQM.tipo=1;
                       BM.add(BQM);
                       t=true;
                       ñ=false;
                       cont=1;
                       longitudes=0;
                       Indicadores.clear();
                       }
                   }
                 
                   
                   if(t){
                       BM.add(p);
                       
                   
                   }
                                  
                  
                   if(tamaño==bloque.size()){
                   BM.add(p);
                   }
                   
                } 
            
       return BM;     
}
public double PromedioFracion(  ArrayList<BloqueMemoria> bloque){
        int cont=0;
        double Sumador=0;
    for(BloqueMemoria p: bloque){
                cont=1+cont;
                if(bloque.size()!=cont){
                if(p.tipo==1){
                    Sumador=p.longitud+Sumador;
                
                }
                }
            
            }
    Sumador=Sumador/10000;
    

return Sumador;

}




            public ArrayList<BloqueMemoria> IncrementarHeap( ArrayList<BloqueMemoria> bloque, int id){
            ArrayList<BloqueMemoria> BM=new ArrayList<>();
            
            Estrategias Estrateg= new Estrategias();
            random rand= new random();
            BloqueMemoria bloquem =new BloqueMemoria();
            BloqueMemoria bloquem2 =new BloqueMemoria();
            int longitud=rand.uniforme(1,5);        
                if(rand.Bernulli(2)){
                    
                       base=Estrateg.Estrategias(1,longitud , bloque);
                       bloquem.idproceso=id;
                       bloquem.base=base;
                       bloquem.tipo=2;
                       bloquem.longitud=longitud; 
                    }
                
                
                     for(BloqueMemoria k: bloque){
                         if(k.base==base){
                            BM.add(bloquem);
                      
                             if (k.longitud!=longitud) {
                                 bloquem2.base=bloquem.base+bloquem.longitud;
                                 bloquem2.longitud  =k.longitud-bloquem.longitud;
                                 bloquem2.tipo=1;
                                 BM.add(bloquem2);
                             }
                             
                         
                         }else {
                             
                         BM.add(k);
                         
                         } 
                     
                     }System.out.println(bloque);
                                       
                
       return BM;     

}





public ArrayList<BloqueMemoria> CargarSistemaOPerativo( ArrayList<BloqueMemoria> bloque){
            BloqueMemoria bloquem1 =new BloqueMemoria();
            BloqueMemoria bloquem =new BloqueMemoria();
                       bloquem.idproceso=0;
                       bloquem.base=0;
                       bloquem.tipo=0;
                       bloquem.longitud=2000; 
                       bloque.add(bloquem);
                       bloquem1.base=bloquem.base+bloquem.longitud;
                       bloquem1.longitud =10000;
                       bloquem1.tipo=1;
                       bloque.add(bloquem1);
       return bloque;     
}}
