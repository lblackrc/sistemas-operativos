/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package so;

import Random.random;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author haobr
 */



public class Proceso {

        int bursttime;
        int interrupciones;
        int prioridad;
        int id;
        int peso;
        int periodoInterrupcion;
        String estado;
        int tiempoArribo;
        int tiempoInterrupcion;
        int tiempoespera;
        int tiemporetorno;
        String codError;
        int idDES;
        int PC;
        int ProcesoPadre;
    public Proceso(int id)  {
        this.ProcesoPadre=0;
        random r=new random();
        this.tiempoArribo=r.exponencial(100);
        this.bursttime =r.exponencial(100);
        this.interrupciones = r.Ninterrupciones(bursttime);
        this.tiempoInterrupcion=0;//
        this.prioridad = r.exponencial(20);
        this.id = id;//
        this.peso = this.bursttime/2;
        this.periodoInterrupcion = this.bursttime/this.interrupciones;
        this.estado = "iniciado";
        this.tiempoespera=0;
        this.tiemporetorno=0;
        this.codError="";
        this.idDES=0;
        this.PC=this.id*1000;        
    }

    public Proceso(int id, ArrayList<Proceso> process)  {
        this.ProcesoPadre=0;
        random r=new random();
        this.tiempoArribo=r.TiempoEntreArribos(100, process);
        this.bursttime =r.exponencial(100);
        this.interrupciones = r.Ninterrupciones(bursttime);
        this.tiempoInterrupcion=0;//
        this.prioridad = r.exponencial(20);
        this.id = id;//
        this.peso = this.bursttime/2;
        this.periodoInterrupcion = this.bursttime/this.interrupciones;
        this.estado = "iniciado";
        this.tiempoespera=0;
        this.tiemporetorno=0;
        this.codError="";
        this.idDES=0;
        this.PC=this.id*1000;        
    }

    
    
    
    public int getIdDES() {
        return idDES;
    }

    public void setIdDES(int idDES) {
        this.idDES = idDES;
    }

    public int getPC() {
        return PC;
    }

    public void setPC(int PC) {
        this.PC = PC;
    }

    public String getCodError() {
        return codError;
    }

    public void setCodError(String codError) {
        this.codError = codError;
    }

    public int getTiempoespera() {
        return tiempoespera;
    }

    public void setTiempoespera(int tiempoespera) {
        this.tiempoespera = tiempoespera;
    }

    public int getTiemporetorno() {
        return tiemporetorno;
    }

    public void setTiemporetorno(int tiemporetorno) {
        this.tiemporetorno = tiemporetorno;
    }

    public int getTiempoInterrupcion() {
        return tiempoInterrupcion;
    }

    public void setTiempoInterrupcion(int tiempoInterrupcion) {
        this.tiempoInterrupcion = tiempoInterrupcion;
    }

    public int getTiempoArribo() {
        return tiempoArribo;
    }

    public void setTiempoArribo(int tiempoArribo) {
        this.tiempoArribo = tiempoArribo;
    }

    public int getBursttime() {
        return bursttime;
    }

    public void setBursttime(int bursttime) {
        this.bursttime = bursttime;
    }

    public int getInterrupciones() {
        return interrupciones;
    }

    public void setInterrupciones(int interrupciones) {
        this.interrupciones = interrupciones;
    }

    public int getPrioridad() {
        return prioridad;
    }

    public void setPrioridad(int prioridad) {
        this.prioridad = prioridad;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPeso() {
        return peso;
    }

    public void setPeso(int peso) {
        this.peso = peso;
    }

    public int getPeriodoInterrupcion() {
        return periodoInterrupcion;
    }

    public void setPeriodoInterrupcion(int periodoInterrupcion) {
        this.periodoInterrupcion = periodoInterrupcion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
       

    
    
    
    
    
    @Override
    public String toString() {
        return "procesos{" + " id=" + id + "  bursttime=" + bursttime  +"   tiempo restante : "+tiempoInterrupcion + "   id dispositivo : "+ idDES+" id ProcesoPadre: "+ProcesoPadre+  '}'    ;
    }

}